﻿using static System.Net.Mime.MediaTypeNames;

namespace Heart
{
	internal class Program
	{
		static void Main(string[] args)
		{
			DrawHeartAndText();

			SimulateFireworks();
		}

		static void SimulateFireworks()
		{
			Random rand = new Random();
			
			string[] fireworks = { "*", "+", "x", ".", "o" };

			for (int i = 0; i < 100; i++)
			{
				Console.SetCursorPosition(rand.Next(Console.WindowWidth), rand.Next(Console.WindowHeight));
				Console.ForegroundColor = (ConsoleColor)rand.Next(1, 16);
				Console.Write(fireworks[rand.Next(fireworks.Length)]);
				
				Thread.Sleep(100);
			}

			Console.ResetColor();
			Console.SetCursorPosition(0, Console.WindowHeight - 1);
		}

		static void DrawHeartAndText()
		{
			string[] heartLines = {
			"     *****       *****     ",
			"   *********   *********   ",
			" ************* ************* ",
			"*****************************",
			" *************************** ",
			"  *************************  ",
			"   ***********************   ",
			"    *********************    ",
			"     *******************     ",
			"       ***************       ",
			"         ***********         ",
			"           *******           ",
			"             ***             ",
			"              *              "
		};

			int consoleWidth = Console.WindowWidth;
			
			foreach (string line in heartLines)
			{
				int leftPadding = (consoleWidth - line.Length) / 2; 
				
				Console.WriteLine(line.PadLeft(line.Length + leftPadding));
			}

			Console.WriteLine();

			string text = "I love C#! Su Valentino diena! :D";
			
			int textLeftPadding = (consoleWidth - text.Length) / 2;

			Console.WriteLine(text.PadLeft(text.Length + textLeftPadding));
		}
	}
}